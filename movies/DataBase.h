 

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Movie.h"
@interface DataBase : NSObject
@property (strong , nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *favoriteDB;
@property NSString *status;
@property NSMutableArray *myfavorites;
-(int)insertfav:(NSString*)fid:(NSString*)ftitle:(NSString*)frelease_date:(NSString*)fposter_path:(NSString*)foverview:(NSString*)fvote_average:(NSString*)ftrailer;
-(NSMutableArray*)loadall;
-(void)removefavorite:(NSString*)fid;
+(DataBase*) sharedInstance;
@end
